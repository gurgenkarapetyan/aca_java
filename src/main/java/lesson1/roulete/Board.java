package lesson1.roulete;

import java.util.concurrent.ThreadLocalRandom;

public final class Board {
    int num;
    String color;

    public Board(){}

    public void generate(){

        num = ThreadLocalRandom.current().nextInt(37);

        if(num == 0)
            color = "None";
        if(num%2 == 0)
            color  = "Red";
        else
            color = "Black";
    }


    public int getNum() {
        return num;
    }

    public String getColor() {
        return color;
    }
}
