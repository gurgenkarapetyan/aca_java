package lesson1.roulete;

import java.util.Random;

public final class Player {
   private Random rand = new Random();
   private int budget;

   public Player(int budget) {
      this.budget = budget;
   }

   public void addToBudget(int amount) {
      budget += amount;
   }

   public void removeFromBudget(int amount) {
      budget -= amount;
   }

   public int getBudget() {
      return budget;
   }

   public String bet() {

//        int bet_num = (int)Math.random()*37;
      int bet_num = rand.nextInt(2);

      if (bet_num % 2 == 0)
         return "Red";

      return "Black";
   }
}
