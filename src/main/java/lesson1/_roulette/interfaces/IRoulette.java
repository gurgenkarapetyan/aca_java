package lesson1._roulette.interfaces;

public interface IRoulette {

   enum Color {
      GREEN,
      RED,
      BLACK
   }

   interface IResult {
      Color getColor();

      int getNumber();
   }

   IResult spin();
}
